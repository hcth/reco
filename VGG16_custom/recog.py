import os,sys, shutil
currentdir = os.path.dirname(os.path.realpath(__file__))
main_path = os.path.dirname(currentdir)
sys.path.append(main_path)
from tensorflow.keras.models import load_model
import numpy as np
import argparse
import imutils
import cv2


def recog(filenames):
    """This function takes a list of paths to images as an argument, and returns a list of labels predicted my the VGG16_custom model"""
    # load the trained model from disk. Acc ~99% during fitting
    print("[INFO] loading model...")
    model_path = os.path.sep.join([main_path,'VGG16_custom','VGG16custom.model'])
    model = load_model(model_path)
    CLASSES_en = np.array(["Apple_golden", "Bacon_cubes","Banana","Carrot","Cod_fillets",
        "Eggplant","Green_pepper","Oranges","Pear","Red_pepper","Potato",
        "Raw_beef","Raw_chicken","Raw_pasta","Rice","Salmon_raw_piece","Strawberry","Sugar_cube",
        "Tomato","Zucchini"]) # Classes effectively used during training
    CLASSES_fr = np.array(["pomme", "lardon","banane","carotte","cabillaud",
        "aubergine","poivron vert","orange","poire","poivron rouge","pomme de terre",
        "viande","poulet","pâtes","riz","saumon","fraise","sucre",
        "tomate","courgette"])
    predicted_labels_fr = [] # In french because we use a database that scrapped marmiton.fr
    
    for filename in filenames:
        # loading the input image
        image = cv2.imread(filename)
        # swapping pixels + resizing to VGG16-compatible size
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = cv2.resize(image, (224, 224))
        # convert the image to a floating point data type and perform mean
        # subtraction
        image = image.astype("float32")
        mean = np.array([123.68, 116.779, 103.939][::1], dtype="float32")
        image -= mean
        # pass the image through the network to obtain our predictions
        preds = model.predict(np.expand_dims(image, axis=0))[0]
        i = np.argmax(preds)
        label = CLASSES_fr[i]
        predicted_labels_fr.append(label)
    return predicted_labels_fr


def test_recog():
    # given
    path_data = 'VGG16_custom/data_test'
    photos_test = os.listdir(path_data)
    filenames = [os.path.sep.join([main_path,path_data,photo]) for photo in photos_test]
    exact_labels = ["pâtes","viande","tomate"]
    # when
    predicted_labels = recog(filenames)
    # then
    assert predicted_labels == exact_labels, "The model is not precise enough ..."
    print("The function recog works fine !")


if __name__ == "__main__":

    ### for a "difficult" set of images
    # path_data = 'VGG16_custom/data_hard'
    # photos_test = os.listdir(path_data)
    # filenames = [os.path.sep.join([main_path,path_data,photo]) for photo in photos_test]
    # predicted_labels = recog(filenames)
    # for i in range(len(photos_test)):
    #     print(f"{photos_test[i]} has been seen as {predicted_labels[i]}")

    ### TDD
    test_recog()
