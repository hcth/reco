import os,sys, shutil
currentdir = os.path.dirname(os.path.realpath(__file__))
main_path = os.path.dirname(currentdir)
sys.path.append(main_path)
import cv2
import imutils
from math import *
import operator

import numpy

#image path
imgpath = main_path + r"\Recog_image\photos_test\poulet.jpg"
finalpath = main_path + "/Recog_image/storage/"


def crop_image_with_certain_parameters(imgpath,sigma,finalpath):
    img = cv2.imread(imgpath)
     
    def smoothing_image(img,sigma): # Pour lisser une image (lowering Gaussian noise)
        """img is a 3D-array, sigma is a 2-tuple"""
        blurred = cv2.GaussianBlur(img,sigma,0)
        return blurred

    # On lisse les contrastes pour améliorer la recherche de contours
    flou=smoothing_image(img,sigma)
    # On passe en nuance de gris puis en image bianire
    gray = cv2.cvtColor(flou,cv2.COLOR_BGR2GRAY)
    th= cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,71,17)
    # #applying canny edge detection
    edged = cv2.Canny(th, 1, 2)
    #finding contours
    (contour) = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    contour = imutils.grab_contours(contour)

    idx = 0
    L=[]
    for c in contour:
        x,y,w,h = cv2.boundingRect(c)  
        if w>15 and h>15:
            idx+=1
            L.append([idx,h*w,(x,y,w,h)])

    # On trie la liste afin de n'obtenir que celle avec la plus grande aire donc les objets intéressants
    sorted_list = sorted(L, key=operator.itemgetter(1))
    sorted_list=sorted_list[-5:]

    idx = 0
    for k in sorted_list:
        idx+=1
        x,y,w,h=k[2]
        new_img=img[y:y+h,x:x+w]  #On Crop les images aux limites des contours
        path=finalpath+'/'+str(sigma)+str(idx)+ '.png'
        print(path)
        cv2.imwrite(path, new_img)

    # cv2.drawContours(img, contour, -1, (0,255,0), 3)
    # cv2.imshow("Original Image",img)
    # cv2.imshow("Canny Edge",edged)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    print("Cropped successfully")

def test_crop_image():
    try:
        crop_image_with_certain_parameters(imgpath,(3,3),finalpath)
    except cv2.error:
        print("erreur de path")
    list_crops = os.listdir(finalpath)
    assert list_crops 
    print('il y a bien des éléments dans le dossier de stockage')
    first_image = cv2.imread(os.path.sep.join([finalpath,list_crops[0]]))
    assert type(first_image) == numpy.ndarray
    print("il s'agit bien d'images")
    assert first_image.size != 0
    print("les images ne sont pas vides")

if __name__ == "__main__":
    crop_image_with_certain_parameters(imgpath,(7,7),finalpath)
    # crop_image_with_certain_parameters(imgpath,(5,5),finalpath)

