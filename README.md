# Ingredients_matching

This project is aimed at determining the possible recipes from a picture of several ingredients or several pictures of a single ingredient.

# Members

### Jean Arbash
### Christophe Doustaly
### Dan Revcolevschi
### Bruno Tabet
### Hugo Chan-To-Hing
### Alexandre Herment

# Usage

- Install the packages

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install tensorflow, keras, flask, opencv, bs4, urllib, sqlite3 and imutils.

```bash
pip install tensorflow
pip install keras
pip install flask
pip install opencv
pip install bs4
pip install urllib
pip install sqlite3
pip install imutils
```

To initialize the website, you have to type in the console `git submodule update --init --recursive`.

- Run the code

The only useful file is "website/app.py". You just need to type in the console ``python website_reco/app.py`` and then click on the link that appears in the console to have access to the website.
Then follow the instructions in the website to upload the pictures of your ingredients and wait for your tasty recipes !
In the directory "data", you can find a few pictures to test the application. If you want to take your own pictures, make sure they are distinct enough from the background. Only 20 ingredients are supported, read VGG16_custom/recog.py for further information.

# Explanations

- The object recognition 

The test pictures are stored in "Recog_image/photos_test". You only have to open "Recog_image/crop_image_def.py" , replace at the beginning the imgpath by the path of the image you want to crop and run ``python Recog_image/crop_image_def.py`` to get several cropped images in the folder storage !

- The neural network

All the useful files are stored in VGG16_custom. The model "VGG16custom.model" file contains the state of the neural network and you only have to run ``python VGG16custom/recog.py`` after writing in the file in the ``if  __name__ == "__main__": ``  `` recog(a_list_of_pathes_to_images) `` with a_list_of_pathes_to_images being the pathes of your images stored in a list to know what ingredients are in every images !

- The database

All the functions are stored in Recettes. You can try to import recipes from Marmiton by yourself by running ``python Recettes/B_database.py``, all the informations would then be stored in a database called Recettes.db !


