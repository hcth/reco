from urllib import request
from bs4 import BeautifulSoup

# POUR TOUTES LES FONCTIONS, L'ARGUMENT EST UN URL ENTRE GUILLEMETS

# On récupère et on parcourt les information de la page web voulue
def scraping(URL):
    pageResponse = request.urlopen(URL)
    pageData = pageResponse.read()
    return pageData

# A l'aide d'un URL de recette, on retourne une liste des ingrédients
def ingredients(URL):
    pageSoup = BeautifulSoup(scraping(URL), 'html.parser')
    raw2 = pageSoup.find_all('p',attrs ={'class': "name_singular"} ) #On trouve étape par étape les endroits de la page Web qui nous intéressent
    paragraphs = []
    for x in raw2:
        if str(x) not in paragraphs:
            paragraphs.append(str(x))
    ingredients = []
    for x in paragraphs:
        pos1 = x.find('data-name-singular="')
        pos2 = x.find('" style')
        ing = (x[pos1:pos2])[20:]
        if ing[0] == ' ':
            ing = ing[1:]
        ingredients.append(ing)
    return ingredients

# On va chercher la note de la recette 
def rating_recipe(URL):
    pageSoup = BeautifulSoup(scraping(URL), "html.parser")
    raw = pageSoup.find_all('span', attrs = {'class': "recipe-infos-users__rating"})
    text = str(raw)
    text = text[42:-10]
    return text

# A l'aide d'un URL de Recette, on renvoie le titre de la recette sous forme de chaîne de caractères
def name_recipe(URL):
    pageSoup = BeautifulSoup(scraping(URL), 'html.parser')
    raw = pageSoup.find_all('h1')
    title = str(raw)
    pos1 = title.find('[<h1 class="main-title">\n') #ATTENTION: il y a un espace à supprimer après main-title
    pos2 = title.find('</h1>]')
    title = (title[pos1:pos2])[26:]
    while title[0] == ' ':
        title = title[1:]
    while title[-1]== ' ':
        title = title[:-1]
    return title

# On récolte les deux infos importante de chaque recette
def data(URL):
    return (name_recipe(URL),ingredients(URL))

# Comme, on va se servir d'un lien au hasard, on crée cette fonction afin de récupérer l'adressedu lien sur lequel on est arrivé
def adress_url(URL):
    pageSoup = BeautifulSoup(scraping(URL), "html.parser")
    raw = pageSoup.find_all()
    titre = str(raw)
    pos1 = titre.find('"@context":"http://schema.org","@type":"ItemList","url":')
    pos2 = titre.find('"numberOfItems"')
    return (titre[pos1:pos2])[57:-2]

