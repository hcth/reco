import os,sys, shutil
currentdir = os.path.dirname(os.path.realpath(__file__))
main_path = os.path.dirname(currentdir)
sys.path.append(main_path)
from A_collect_data import *
from B_database import *
from C_query_recipe import *

def test_recup_data():
    URL='https://www.marmiton.org/recettes/recette_cordon-bleu_27731.aspx'
    dat=data(URL)
    link=adress_url(URL)
    assert str(dat[0])=='Cordon bleu'
    print('Le scrapping du nom de la recette fonctionne')
    assert str(dat[1][4])=='oeuf'
    print('Le scrapping des ingrédients fonctionne')
    assert link==URL
    print('Le scrapping des URL fonctionne')

def test_creation_table():
    table=os.listdir(main_path)
    assert 'Recettes.db' in table
    print("La table est bien créée ")

def test_typerecette():
    URL='https://www.marmiton.org/recettes/recette_cordon-bleu_27731.aspx'
    cat=category_recipe(URL)
    dif=difficulty_recipe(URL)
    rating=rating_recipe(URL)
    assert cat== 'Plat principal'
    print("La fonction catégorie marche bien")
    assert dif==1
    print("La fonction difficulté marche bien")
    assert rating=="4.8"
    print("La fonction rating marche bien")

def test_update_database():
    conn= sqlite3.connect('Recettes.db')
    c=conn.cursor()
    mySql_insert_query = """SELECT recettes.nom_recette FROM recettes WHERE id=0"""
    c.execute(mySql_insert_query)
    a=c.fetchall()
    
    assert a[0][0]=='Cordon bleu'
    print("La database est bien mise à jour")

def test_request():
    mySql_insert_query = """SELECT nom_ingredient FROM contient WHERE id=0"""
    c.execute(mySql_insert_query)
    a=c.fetchall()
    list_ingredients=[('tranche de blanc de poulet',), ('tranche de bacon',), ('tranche de fromage',), ('cuillère de farine',), ('oeuf',), ('cuillère de chapelure',), ('g de beurre',), ('cuillère à soupe de farine',), ('cuillère à soupe de chapelure',)]
    print(a)
    assert a == list_ingredients
    print("Les requetes SQL fonctionnent bien")

# # To uncomment before testing
# URL='https://www.marmiton.org/recettes/recette_cordon-bleu_27731.aspx'
# update_database_with_URL (URL,0)
# URL='https://www.marmiton.org/recettes/recette_lasagnes-a-la-bolognaise_18215.aspx'
# update_database_with_URL (URL,1)
# URL = "https://www.marmiton.org/recettes/recette_pates-bolognaise_165724.aspx"
# update_database_with_URL (URL,2)
# URL = "https://www.marmiton.org/recettes/recette_verrine-banane-fraise_57195.aspx"
# update_database_with_URL (URL,3)

if __name__=="__main__":
    conn.commit()
    test_creation_table()
    test_recup_data()
    test_typerecette()
    test_update_database()
    test_request()  
    conn.close()    