import os,sys, shutil
currentdir = os.path.dirname(os.path.realpath(__file__))
main_path = os.path.dirname(currentdir)
sys.path.append(main_path)
from Recettes.A_collect_data import *
from Recettes.B_database import *

#This function returns the various URL links from a page of the "Recettes par catégorie" of the website Marmiton
def recipes_in_page(page_principale):
    liste_URL = []
    pageSoup = BeautifulSoup(scraping(page_principale), 'html.parser')
    raw = pageSoup.find_all('a', attrs = {'class': "recipe-card-link"})
    texte = str(raw)
    acc =0
    for j in range(0,29):
        pos = texte.find(' <a class="recipe-card-link"') + 35
        texte = texte [pos:]
        pos2 = texte.find(">\n")
        texte = texte[:pos2-1]
        liste_URL.append(texte)
        texte = str(raw)
        acc += pos+pos2
        texte = texte[(acc+100):]
    return liste_URL

# The 3 next functions return the links to all the pages of the "Recettes par catégorie" portion of the website
def list_pages_principales_main_dishes():
    liste_pages = []
    for j in range(0,3):
        for i in range(1,10):
            liste_pages.append('https://www.marmiton.org/recettes/index/categorie/plat-principal/'+str(i)+'?rcp='+str(j))
    return liste_pages

def list_pages_principales_starters():
    liste_pages = []
    for j in range(0,3):
        for i in range(1,10):
            liste_pages.append('https://www.marmiton.org/recettes/index/categorie/entree/'+str(i)+'?rcp='+str(j))
    return liste_pages

def list_pages_principales_dessert():
    liste_pages = []
    for j in range(0,3):
        for i in range(1,10):
            liste_pages.append('https://www.marmiton.org/recettes/index/categorie/dessert/'+str(i)+'?rcp='+str(j))
    return liste_pages

#returns a list of recipes that can be found in the page "Recettes par catégories", approximately 2000 recipes
def links_recipes_dishes():
    plats = []
    pages = list_pages_principales_main_dishes() + list_pages_principales_starters() + list_pages_principales_dessert()
    for i in range (0,len(pages)):
        nouveaux_plats = recipes_in_page(pages[i])
        for j in range (len(nouveaux_plats)):
            URL = nouveaux_plats[j]
            if URL not in plats: #We make sure to not have twice the same recipe in the database
                plats.append(URL)
    return plats
list_url=links_recipes_dishes()
conn= sqlite3.connect('Recettes.db')
c=conn.cursor()
#We use the function update_database_with_URL to fill the database by going through the list of URL found previously on the main page
def insert_recipes_in_database_2(lis):
    list_URL = lis #links_recipes_dishes()
    n = len(list_URL)
    mySql_insert_query = "SELECT MAX(id) from recettes"
    c.execute(mySql_insert_query)
    result=c.fetchall()
    if result[0][0]==None:
        id=1
    else:
        id=int(result[0][0])+1
    for i in range (10):
        try :
            URL=list_URL[i]
            update_database_with_URL(URL,id)
            conn.commit()
            id += 1
        except:
            pass



#-----------------------TESTS---------------------------
def test_recipes_in_page():
    page_principale = 'https://www.marmiton.org/recettes/index/categorie/plat-principal/3?rcp=0'
    liens_recettes = recipes_in_page(page_principale)
    assert liens_recettes[0] == 'https://www.marmiton.org/recettes/recette_coq-au-vin-maison_25755.aspx'
    print('The scrapping of recipes on the main pages works') 

def test_links_main_page():
    pages = list_pages_principales_main_dishes() + list_pages_principales_starters() + list_pages_principales_dessert()
    assert len(pages) == 81
    print('We successfully generated the links to the main pages')

# ----------------------------------------------------------

if __name__=="__main__":
    test_recipes_in_page()
    test_links_main_page()
    insert_recipes_in_database_2(list_url)
    conn.close()
