import os,sys, shutil
currentdir = os.path.dirname(os.path.realpath(__file__))
main_path = os.path.dirname(currentdir)
sys.path.append(main_path)
import sqlite3
from Recettes.A_collect_data import *
conn= sqlite3.connect('Recettes.db')
c=conn.cursor()
 
c.execute(""" CREATE TABLE IF NOT EXISTS recettes (
 id varchar(42),
 nom_recette varchar(32),
 url_recette varchar(32),
 categorie varchar(32),
 difficulte varchar(32),
 note varchar(42),
 PRIMARY KEY (id))""")
 
c.execute(""" CREATE TABLE IF NOT EXISTS ingredients (
 nom_ingredient varchar (32),
 PRIMARY KEY (nom_ingredient))""")
 
c.execute("""CREATE TABLE IF NOT EXISTS contient (
id varchar (32),
nom_recette varchar (32),
nom_ingredient varchar (32),
FOREIGN KEY (id) REFERENCES recettes(id)
FOREIGN KEY (nom_ingredient) REFERENCES ingredients(nom_ingredients))""")

# On va chercher sur le site marmiton la catégorie de chaque type de recette
def category_recipe(URL):
    pageSoup = BeautifulSoup(scraping(URL), 'html.parser')
    raw = pageSoup.find_all('li', attrs = {'class': "mrtn-tag"})
    text = str(raw[0])
    text = text[21:-5]
    return text

# print(categorie_recette('https://www.marmiton.org/recettes/recette_tarte-aux-pommes_18588.aspx'))
# On ajoute la difficulté de la recette
def difficulty_recipe(URL):
    pageSoup = BeautifulSoup(scraping(URL), 'html.parser')
    raw = pageSoup.find_all('span', attrs = {'class': "recipe-infos__item-title"})
    text=str(raw[2])
    text = text[39:-7]
    if text == "très facile":
        return 1
    if text == "facile":
        return 2
    if text == "Niveau moyen":
        return 3
    if text =="difficile":
        return 4

# On implémente la fonction de remplissage des différentes tables de notre base de donnée SQL
def update_database_with_URL (URL,identifiant):
    recipe=name_recipe(URL)
    list_ingredients=ingredients(URL)
    cat=category_recipe(URL)
    dif=difficulty_recipe(URL)
    rating=rating_recipe(URL)
    mySql_insert_query = """INSERT INTO recettes (id,nom_recette, url_recette,categorie,difficulte,note) VALUES (?,?,?,?,?,?);"""
    recordTuple = (identifiant,recipe, URL,cat,dif,rating)
    try:
        c.execute(mySql_insert_query, recordTuple)
    except: #On évite tout problème d'intégrité afin d'éviter les doublons
        pass
    n=len(list_ingredients)
    for i in range(n):
        mySql_insert_query = """INSERT INTO ingredients (nom_ingredient) VALUES (?);"""
        recordTuple = (list_ingredients[i],)
        try:
            c.execute(mySql_insert_query, recordTuple)
        except:
            pass
        mySql_insert_query = """INSERT INTO contient (id,nom_recette,nom_ingredient) VALUES (?,?,?);"""
        recordTuple = (identifiant,recipe,list_ingredients[i],)
        try:
            c.execute(mySql_insert_query, recordTuple)
        except:
            pass

 
# On remplit effectivement la data_base avec tous les liens provenant du lien des recettes au hasard
def insert_all_database ():
    conn= sqlite3.connect('Recettes.db')
    c=conn.cursor()
    mySql_insert_query = "SELECT MAX(id) from recettes"
    c.execute(mySql_insert_query)
    result=c.fetchall()
    # print(result)
    if result[0][0]==None:
        id=1
    else:
        id=int(result[0][0])+1 
    for i in range(5):
        URL= 'https://www.marmiton.org/recettes/recette-hasard.aspx'
        URL=adress_url(URL)
        print(URL)
        try:
            update_database_with_URL(URL,id)
            conn.commit()
            id+=1
        except:
            pass

    
    

if __name__=="__main__":
    insert_all_database()
    conn.commit()
    conn.close()


# 'https://www.marmiton.org/recettes/recette-hasard.aspx'