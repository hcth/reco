import os,sys, shutil
currentdir = os.path.dirname(os.path.realpath(__file__))
main_path = os.path.dirname(currentdir)
sys.path.append(main_path)
from Recettes.B_database import *

#On travaille sur plusieurs ingrédients et on affine petit à petit la recherche de recette en les confrontant pour différents aliments
#l'argument ingredients est une liste qui a au moins un élément
def request_order(ingredients,ordre): #Ordre doit être entre croissant ou decroissant
    conn= sqlite3.connect('Recettes.db')
    c=conn.cursor()
    mySql_insert_query = "SELECT DISTINCT recettes.nom_recette,recettes.url_recette,categorie,difficulte,note from contient JOIN recettes ON contient.nom_recette = recettes .nom_recette WHERE contient.nom_ingredient LIKE (?)"
    my_query = mySql_insert_query
    n = len(ingredients)
    tuple_ing = ('%'+ingredients[0]+'%',)
    for i in range (1,n):
        my_query = my_query + " INTERSECT " + mySql_insert_query
        tuple_ing += ('%'+ingredients[i]+'%',)
    if ordre=="croissant":
        my_query=my_query+ "ORDER BY difficulte ASC"
    elif ordre=="decroissant":
        my_query=my_query+ "ORDER BY difficulte DESC"
        
    c.execute(my_query, tuple_ing)
    dan=c.fetchall()
    return dan

def request_rating(ingredients):
    conn= sqlite3.connect('Recettes.db')
    c=conn.cursor()
    mySql_insert_query = "SELECT DISTINCT recettes.nom_recette,recettes.url_recette,categorie,difficulte,note from contient JOIN recettes ON contient.nom_recette = recettes .nom_recette WHERE contient.nom_ingredient LIKE (?)"
    my_query = mySql_insert_query
    n = len(ingredients)
    tuple_ing = ('%'+ingredients[0]+'%',)
    for i in range (1,n):
        my_query = my_query + " INTERSECT " + mySql_insert_query 
        tuple_ing += ('%'+ingredients[i]+'%',)
    my_query+="ORDER BY note DESC"
    c.execute(my_query, tuple_ing)
    dan=c.fetchall()
    return dan


if __name__=="__main__":
    ingredients=['sucre']
    print(request_order(ingredients,"croissant"))